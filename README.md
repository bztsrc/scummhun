Klasszikus kalandjátékok MAGYARUL
=================================

Ezek az adatfájlok ScummVM-hez használhatók, futtatható nincs bennük, így garantáltan vírusmentesek.
A következő játékok soha ki nem adott, nem hivatalos magyar verziói:

- monkey_hun.zip: A Majomsziget Titka, egy lelkes fiatal kalóz szeretne lenni, fergeteges poénokkal
- dott_hun.zip: A Csáp Napja, ha ezt nem ismered, akkor nem tudod, mi az "a kalandjáték"...
- dig_hu.zip: Az Ásatás, a név ellenére tudományos fantasztikus, egy idegen bolygón kell nyomozni
- atlantis_hun-zip: Indy és az Elveszett Atlantisz, a híres régészprofesszorral nyomozhatunk Atlantisz után
- loom_hun.zip: Égi Szövőszék, fantasy, egy varázslótanoncot alakítunk, akinek meg kell mentenie a világot a pusztulástól
- amazon_hun.zip: Az Amazon Királynő Röpte, egy pilótával kell nyomozni a XX. század közepén Brazíliában

A Loom nagyon régi, ebben sajnos még nincsenek feliratozva a beszédek, így csak a felület kerülhetett lefordításra.
Az összes többi játékban mind a felület, mind a feliratok le lettek fordítva, sajnos magyar szinkron nincs. A fordítások
Selmeci Béla és Dunstan munkái, én csak végigszoptam a magyarítás technikai oldalát, a ScummVM fájlok konverzióját
és szerkesztését. Elég pilótavizsgás volt, az eredeti fordítószkriptek több játék esetén is elrontották a fájlokat,
így manuálisan kellett a cseréket elvégezni, ami aligha várható el egy egyszeri játékostól. Ezért arra gondoltam,
megosztom a végeredményt, hátha mást is érdekel.

Még várható több játék, jelenleg a Törött Kard sorozattal szívok, amiben a Templomos lovagok titkára kell fényt
deríteni. Sajnos az a verzió, ami nekem megvan, illetve az, amihez létezik fordítás, nem igazán kompatíbilis, vagy
sehogy sem akar egyelőre ScummVM alatt elindulni. Ha sikerült végre megoldanom, felrakom azokat is.

Remélem más is legalább annyira élvezni fogja magyarul ezeket a klasszikusokat, mint a gyerekeim! :-)
